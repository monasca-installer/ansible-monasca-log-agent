# Monasca-Log-Agent

Installs the Monasca-Log-Agent on a debian system.

## Requirements

JRE must be installed

## Configuration parameter

### Configuration

| name | description | type | required | default | example |
| --- | --- | --- | --- | --- | --- |
| monasca_log_api_url | monasca log api url | string | true | | `https://192.168.10.4:5607/v3.0` |
| monasca_log_api_insecure | set to true if monasca-log-api is using an insecure ssl certificate otherwise false | boolean | false | | |
| keystone_url | keystone api url | string | true | | `http://192.168.10.5:35357/v3` |
| keystone_api_insecure | set to true if keystone is using an insecure ssl certificate | boolean | false | | |
| monasca_log_agent_project | Keystone user credentials: project name | string | true | | monasca |
| monasca_log_agent_user | Keystone user credentials: username | string | true | | admin-agent |
| monasca_log_agent_password | Keystone user credentials: password | string | true | | password |
| user_domain_name | Keystone user credentials: user domain name | string | true | | default |
| project_domain_name | Keystone user credentials: project domain name | string | true | | default |
| global_dims | global array dimensions in form of key-value pairs to describe the monitored node | array | false | | ['app_type:kafka', 'priority:high'] |

log_files - List of files to collect entries from. This can be a list of file paths:

```yaml
log_files:
  - "/var/log/messages"
  - "/var/log/*.log"
```

or mapping, e.g.:

```yaml
log_files:
  - { tags: '["syslog"]', add_field: '{ "dimensions" => { "service" => "system" }}', path: '"{{ log_dir }}/messages"' }
  - { tags: '["monasca"]', add_field: '{ "dimensions" => { "service" => "log-api"
                                                           "language" => "python" }}', path: '"{{ log_dir }}/messages"' }
```

where:

* tags - identifies the type of [search pattern for multiline entries](vars/main.yml)
* add_field - adds dimensions to log file which helps to search in elasticsearch

### installation required parameter

* base_dir_agent - a directory where monasca-log-agent is installed, defaults to '/opt'
* download_timeout - defines a time for timeout when downloading tar files
* logstash_agent_version - defines the logstash version to be installed
* logstash_agent_download - logstash base download url
* logstash_agent_dest - logstash base install path
* log_agent_conf_dir - a directory where logstash configuration files are stored
* logstash_agent_log_file - monasca-log-agent log file path

## License

Apache License, Version 2.0

## Author Information

Kamil Choroba
