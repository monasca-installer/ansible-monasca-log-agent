#!/bin/bash
if [[ `/sbin/init --version 2>&-` =~ upstart ]]; then
  echo upstart;
elif [[ `systemctl 2>&-` =~ -\.mount ]]; then
  echo systemd;
elif [[ -f /etc/init.d/cron && ! -h /etc/init.d/cron ]]; then
  echo sysv-init;
else
  echo unknown;
fi
